// Written by Eric Schultz
// 6/8/2017

// changeTab: provides tab-switching functionality
function changeTab(e, tabName) {
	var i, tabcontent, navtabs;
	
	tabcontent = document.getElementsByClassName("tab-content");
	for(i = 0; i < tabcontent.length; i++) {
		tabcontent[i].style.display = "none";
	}
	
   navtabs = document.getElementsByClassName("navtab");
   for (i = 0; i < navtabs.length; i++) {
       navtabs[i].className = navtabs[i].className.replace(" active", "");
   }

   document.getElementById(tabName).style.display = "block";
   e.currentTarget.className += " active";

}
